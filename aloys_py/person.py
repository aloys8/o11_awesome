# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:54:39 2022

@author: Aloys.Erkelens
"""


class Person:
    def __init__(self, name, last_name, birth_year=None):
        self.name = name
        self.last_name = last_name
        self.full_name = None
        self.birth_year = birth_year
        self.age = None
        
    def get_full_name(self):
        self.full_name = self.name + ' ' + self.last_name
        return self.full_name

    def calculate_age(self):
        self.age = 2022 - self.birth_year()
        return self.age


if __name__ == '__main__':
    you = Person('You', 'Wayne')
    print(you.name, you.last_name)
    print(you.full_name)
