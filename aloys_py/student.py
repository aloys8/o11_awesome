# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 14:22:56 2022

@author: Aloys.Erkelens
"""
from person import Person


class Student(Person):
    def __init__(self, name, last_name, birth_year):
        super().__init__(name, last_name, birth_year)
        self.workshop = None
    
    def enroll(self, workshop):
        self.workshop = workshop
    
    def __str__(self): # DUNDER
        return f'Student {self.name} {self.last_name}'

    
if __name__ == '__main__':
    me = Student('Roderick', 'Derp')
    print(me.name, me.last_name)
    print(me.get_full_name())
    me.enroll('python course')
    print(me.workshop)
    print(me)
